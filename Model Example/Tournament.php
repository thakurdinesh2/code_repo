<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;
use Cviebrock\EloquentSluggable\Sluggable;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Response;
use Auth;
use DB;
class Tournament extends Model
{
    use Sluggable;
    use SoftDeletes;
		/**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'sport_id',
     'banner_image', 'image', 'name', 'slug', 
     'start_date_time', 'end_date_time', 
     'registeration_end_date', 'lat', 'lng','address','city' ,'refree_type', 'security', 'password', 'registration_fee','format'];

     

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id')
                    ->select('id','name','email','image','status')->withCount('tournaments');
    }
    
   

    public function teams()
    {
        return $this->hasMany(Team::class, 'tournament_id', 'id');
    }
     

    #lat lng query
    public function scopeProximity($query, $lat, $lng, $radius, $units)
	{
		$radius = $radius ? $radius : 500;

		if($units == 'KM'){
			$distanceUnit = 111.045;
		}else{
			$distanceUnit = 69.0;
		}

		$haversine = sprintf('tournaments.*, (%f * DEGREES(ACOS(COS(RADIANS(%f)) * COS(RADIANS(lat)) * COS(RADIANS(%f - lng)) + SIN(RADIANS(%f)) * SIN(RADIANS(lat))))) AS distance', $distanceUnit, $lat, $lng, $lat);

		$subselect = clone $query;
		$subselect->selectRaw(DB::raw($haversine)); // Optimize haversine query: http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/

		$latDistance      = $radius / $distanceUnit;
		$latNorthBoundary = $lat - $latDistance;
		$latSouthBoundary = $lat + $latDistance;
		$subselect->whereRaw(sprintf("lat BETWEEN %f AND %f", $latNorthBoundary, $latSouthBoundary));

		$lngDistance     = $radius / ($distanceUnit * cos(deg2rad($lat)));
		$lngEastBoundary = $lng - $lngDistance;
		$lngWestBoundary = $lng + $lngDistance;
		$subselect->whereRaw(sprintf("lng BETWEEN %f AND %f", $lngEastBoundary, $lngWestBoundary));

		$query
			->from(DB::raw('(' . $subselect->toSql() . ') as tournaments'))
			->where('distance', '<=', $radius);
	}
    
}
