<?php
namespace App\Http\Traits;

use Image;
use App\User;
use App\Models\UserTournament;
use App\Models\Notification;
use App\Models\Tournament;
use App\Models\Event;
use App\Models\Sport;
use App\Mail\AddAdminTournament;
use App\Jobs\AddAdminTournamentJob;
use Auth;
use DB;
use Mail;
use Twilio\Rest\Client;

trait CommonTrait
{
    public function imageDynamicName()
    {
        #Available alpha caracters
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(1000000, 9999999)
            . $characters[rand(0, 5)];
        $string = str_shuffle($pin);
        return $string;
    }
    private function userImageVersions($request,$param)
    {   

        $filename = $_FILES[$param]['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        //upload file
        $dynamic_name = round(microtime(true) * 1000).'-'. $this->imageDynamicName() . '.' . $ext;
        $image = $request->file($param)->storeAs('public/images', $dynamic_name);
        if ($image) {
            $image_name = explode('/', $image);
            $name = $image_name[2];
          //  $saved_Image = $this->userImageVersions($image_name[2]);
        }


        $main_dir = storage_path() . '/app/public/images';
        $medium_dir = storage_path() . '/app/public/images/medium';
        $thumb_dir = storage_path() . '/app/public/images/thumb';

        if (!file_exists($thumb_dir)) {
            mkdir($thumb_dir, 0777);
            chmod($thumb_dir, 0777);
        }
        if (!file_exists($medium_dir)) {
            mkdir($medium_dir, 0777);
            chmod($medium_dir, 0777);
        }

        if (file_exists($main_dir . '/' . $name)) {
            chmod($main_dir . '/' . $name, 0777);
            Image::make($main_dir . '/' . $name)->resize(110, 110)->save($thumb_dir . '/' . $name);
            Image::make($main_dir . '/' . $name)->resize(410, 410)->save($medium_dir . '/' . $name);
            chmod($thumb_dir . '/' . $name, 0777);
            chmod($medium_dir . '/' . $name, 0777);
        }
        return $name;
    } 
    public function sendSms($array)
    {
           $accountSid = env('TWILIO_SID');
           $authToken = env('TWILIO_TOKEN');
           $twilioNumber = env('TWILIO_FROM'); 
           $lineBreak = "\n\n";
           $to = $array['country_code'].$array['phone_no'];
           $message = "Hello ".$array['name'].",Welcome to ABC".$lineBreak." OTP :".$array['verify_token'];
           $client = new Client($accountSid, $authToken);
           try {
               $client->messages->create(
                   $to,
                   [
                       "body" => $message,
                       "from" => $twilioNumber
                   ]
               );
               return true;
           } catch (TwilioException $e) {
              return $e;
           }
    }

  

    
    public function sendNotification($user_ids,$tournament_id,$type,$tournament_events_user_id=0,$match_schdule_timing_id=0)
    {   
        
        if($type == 1)
        {
            $sender_id =$tournament_id->user_id;
            $this->notifications($user_ids,$sender_id,$tournament_id->id,$type,$tournament_events_user_id,$match_schdule_timing_id);
            $tournament_id = $tournament_id->id;
        }
        
        if($type == 2 || $type == 3 || $type == 4|| $type == 5 || $type == 6 || $type == 7 || $type == 8 || $type == 9 || $type == 10 || $type == 11 || $type == 12)
        {   
            if(is_array($user_ids))
            {
                $user_ids = $user_ids;
            }
            else
            {
              $user_ids = [$user_ids];
            }
            $sender_id = Auth::id();
            $this->notifications($user_ids,$sender_id,$tournament_id,$type,$tournament_events_user_id,$match_schdule_timing_id);

        }
        
        
         $registrationIds = User::whereIn('id',$user_ids)
                        ->pluck('device_id');
       
        if(count($registrationIds))
        {


        $notification_data = Notification::getStatusVice($sender_id,$tournament_id,$type,$tournament_events_user_id,$match_schdule_timing_id);  
   
        $api_key=env('SECRET_KEY_IOS','sasdas');
        $msg = array
                (
                "title" =>$notification_data['subject'],    
                "id" => '',
                "type" => $type,
                "body" => $notification_data['message'],
                "sender_id" => '',
                "receiver_id" => '',
                "tournament_id" => $tournament_id,
                "tournament_events_user_id" => $tournament_events_user_id,
                "sender_name" => '',
                "order_id" => null,
                "vibrate" => 1,
                "sound" => "default",
                "channel" => "default",
                "priority" => "high",
                "show_in_foreground" => true
            );
        $msg = (object) $msg; 
        $dt = (object) $msg;
        $fields = array
            (
            'registration_ids' =>  $registrationIds,
            'data' =>  $msg,
            'notification' =>  $msg,
            );

        $fields = (object) $fields;    
        $headers = array
            (
            'Authorization: key=' . $api_key,
            'Content-Type: application/json',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch); 
        curl_close($ch); 
        }
    }
 
               
}
