<?php
namespace App\Http\Traits;

use Image;

trait UserTrait
{
    
    

    private function getVerificationCode($length = 12)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    /* function to send a verification email */

    private function sendVerificationMail($register_data)
    {
        $email = trim($register_data["email"]);
        $admin_email = Config::get('variable.ADMIN_EMAIL');
        $frontend_url = Config::get('variable.FRONTEND_URL');
        $user = User::where('email', $email)->first();
        Mail::send('user.register', ['data' => array("verification_token" => $register_data["verification_token"], "email" => $email,
            "frontend_url" => $frontend_url, "name" => $user->name)], function ($message) use ($email, $admin_email) {
            $message->from($admin_email, 'INAR');
            $message->to(trim($email), 'INAR')->subject('INAR : Verify Account');
        });
    }


     private function userProfile($id)
    {
       
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://server.profileservice.com/api/profile/getPersonalProfile', [
             'query' => ['user_id' => $id]
        ]);
        $response = $request->getBody()->getContents();
        return $response;
    }



}
