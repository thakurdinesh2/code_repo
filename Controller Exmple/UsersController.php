<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\ForgotPasswordRequest;
use App\Http\Requests\User\SocialLoginRequest;
use App\Http\Requests\User\UserChangePasswordRequest;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\UserProfileRequest;
use App\Http\Requests\User\UserResendVerifyRequest;
use App\Http\Requests\User\UserSignupRequest;
use App\Http\Requests\User\VerifyOtpRequest;
use App\Http\Requests\User\ResendOtpRequest;
use App\Http\Requests\User\UserResetPasswordRequest;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Http\Traits\CommonTrait;
use App\Http\Traits\UserTrait;
use App\Interfaces\UserInterface;
use App\Mail\SignupEmail;
use App\Mail\ForgotPasswordEmail;
use App\Mail\ChangePasswordEmail;
use App\Models\ActivityLog;
use App\Models\Page;
use App\Models\Notification;
use App\Models\TournamentEventUser;
use App\Role;
use App\User;
use Config;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use Lcobucci\JWT\Parser;
use Mail;
use Response;
use Twilio\Rest\Client;

class UsersController extends Controller implements UserInterface
{
    use CommonTrait, UserTrait;

     
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     *
     *  @SWG\Post(
     *   path="/user/socialLogin",
     *   summary="socialLogin",
     *   consumes={"multipart/form-data"},
     *   produces={"application/json"},
     *   tags={"User"},
     *  @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "name",
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "email",
     *   ),
     *   @SWG\Parameter(
     *     name="device_type",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "Please enter (IOS / ANDROID) ",
     *   ),
     *  @SWG\Parameter(
     *     name="signup_type",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "Please enter (facebook / gmail) ",
     *   ),
     *  @SWG\Parameter(
     *     name="social_id",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "Please enter social id",
     *   ),
     *  @SWG\Parameter(
     *     name="image",
     *     in="formData",
     *     required=false,
     *     type="string",
     *     description = "Please enter image path",
     *   ),
     *   
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Failed"),
     *   @SWG\Response(response=405, description="Undocumented data"),
     *   @SWG\Response(response=500, description="Internal server error")
     * )
     *
     */

    public function socialLogin(SocialLoginRequest $request)
    {
        $requested_data = $request->all();

        $user = User::where('email', '=', request('email'))->first();
        //Now log in the user if exists
        if ($user != null) {

            switch ($user->status) { # if blocked by admin
                case 0:
                    Auth::logout();
                    $data = \Config::get('error.account_not_verified');
                    $data['user_unverified'] = true;
                    $data['data'] = (object) [];
                    $data['user'] = $user;
                    break;
                case 2:
                    Auth::logout();
                    $data = \Config::get('error.account_blocked_admin');
                    $data['data'] = (object) [];
                    return Response::json($data);
                    break;
            }

            DB::table('oauth_access_tokens')->where('user_id', $user->id)->update(['revoked' => true]); # logout

            Auth::loginUsingId($user->id); # login
          
            $user->device_type = isset($requested_data['device_type']) ? $requested_data['device_type'] : '';
            $user->device_id = isset($requested_data['device_id']) ? $requested_data['device_id'] : '';
            $user->signup_type = isset($requested_data['signup_type']) ? $requested_data['signup_type'] : '';
            $user->social_id = isset($requested_data['social_id']) ? $requested_data['social_id'] : '';
            $user->image = isset($requested_data['image']) ? $requested_data['image'] : '';
            $user->save();
            $remember_me = isset($requested_data['remember_me']) ? $requested_data['remember_me'] : false;
            $user_last_login = User::where('id', $user->id)
                ->update(['current_login' => time(), 'last_login' => $user->current_login]);

            $name = $user->fname." ".$user->lname;
            $add_name = $this->add_name_user_tournament($name,$user->id,'',1);    
            return Response::json([
                'status' => 200,
                'message' => 'Account Login successfully.',
                'user' => $user
                ])->header('access_token', $user->createToken(env("APP_NAME"))->accessToken);
        } else {
            $array = [];
            $array['role_id'] = Role::where('name', 'user')->first()->id;
            $array['fname'] = $requested_data['name'];
            $array['name'] = $requested_data['name'];
            $array['email'] = $requested_data['email'];
            $array['device_type'] = isset($requested_data['device_type']) ? $requested_data['device_type'] : '';
             $array['device_id'] = isset($requested_data['device_id']) ? $requested_data['device_id'] : '';
            $array['signup_type'] = isset($requested_data['signup_type']) ? $requested_data['signup_type'] : '';
            $array['social_id'] = isset($requested_data['social_id']) ? $requested_data['social_id'] : '';
            $array['image'] = isset($requested_data['image']) ? $requested_data['image'] : '';
            $array['status'] = 1;
            $array['created_at'] = time();
            $array['updated_at'] = time();

            $user = User::create($array);
            Auth::loginUsingId($user->id);
            $name = $array['name'];
            $add_name = $this->add_name_user_tournament($name,$user->id,'',1);
           
            $remember_me = isset($requested_data['remember_me']) ? $requested_data['remember_me'] : false;
            $user_last_login = User::where('id', $user->id)
                ->update(['current_login' => time(), 'last_login' => $user->current_login]);
            return Response::json([
                'status' => 200,
                'message' => 'Account Login successfully.',
                'user' => $user
                ])->header('access_token', $user->createToken(env("APP_NAME"))->accessToken);

        }
    }
 
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     *
     *  @SWG\Post(
     *   path="/user/varify_otp",
     *   summary="varify_otp",
     *   consumes={"multipart/form-data"},
     *   produces={"application/json"},
     *   tags={"User"},
     *   @SWG\Parameter(
     *     name="phone_no",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "phone_no",
     *   ),  
     *   @SWG\Parameter(
     *     name="otp",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "otp",
     *   ),   
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Failed"),
     *   @SWG\Response(response=405, description="Undocumented data"),
     *   @SWG\Response(response=500, description="Internal server error")
     * )
     *
     */

    public function varify_otp(VerifyOtpRequest $request)
    {
        $phone = $request['phone_no']; 
        $user = User::where('verify_token',$request['otp'])
                    ->where('phone_no',$request['phone_no'])
                    ->where('id',$request['id'])
                    ->update([
                    'status'=>1,
                    'phone_no' => $phone,
                    'verify_token' => ''
                    ]);
        if($user)
        {   
            $requested_data = User::find($request->id);
            if($requested_data)
            {
            $name = $requested_data->fname." ".$requested_data->lname;
            $add_name = $this->add_name_user_tournament($name,$requested_data->id,'',1); 
            }

            Auth::loginUsingId($requested_data->id);
            
            $token= $requested_data->createToken(env("APP_NAME"))->accessToken;
            $user_last_login = User::where('id', $requested_data->id)
                ->update(['current_login' => time(), 'last_login' => $requested_data->current_login]);

            $user_activity_login = ActivityLog::updateOrCreate(
                ['user_id' => $requested_data->id, 'meta_key' => 'last_login'],
                ['user_id' => $requested_data->id, 'meta_key' => 'last_login',
                    'meta_value' => $requested_data->current_login, 'status' => 1,
                    'created_at' => time(), 'updated_at' => time()]
            );

            $name = $requested_data->name;
            $add_name = $this->add_name_user_tournament($name,$requested_data->id,'',1);

            return Response::json([
                'status' => 200,
                'message' => 'Account verified successfully.',
                'user' => $requested_data,
                ])->header('access_token', $token);   
             
            
        }else {
            $data['status'] = 400;
            $data['message'] = 'Please try again with your phone no. with your valid otp.';
            $data['data'] = [];
        } 
        return Response::json($data);
    }

 /**
     * @return \Illuminate\Http\JsonResponse
     *
     *
     *  @SWG\Post(
     *   path="/user/resendOtp",
     *   summary="resendOtp",
     *   consumes={"multipart/form-data"},
     *   produces={"application/json"},
     *   tags={"User"},
     *   @SWG\Parameter(
     *     name="phone_no",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "phone_no",
     *   ),  
     *   @SWG\Parameter(
     *     name="id",
     *     in="formData",
     *     required=true,
     *     type="integer",
     *     description = "id of that particular user",
     *   ), 
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=true,
     *     type="string",
     *     description = "email of that particular user",
     *   ),  
     *   @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     required=false,
     *     type="string",
     *     description = "country_code of phone no",
     *   ), 
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=400, description="Failed"),
     *   @SWG\Response(response=405, description="Undocumented data"),
     *   @SWG\Response(response=500, description="Internal server error")
     * )
     *
     */
    public function resendOtp(ResendOtpRequest $request)
    {
            $phone = $request['phone_no'];
            $user=User::where('id',$request->id)->where('email',$request->email)->first();
            if ($user) {
                $array['verify_token'] = 1234;//mt_rand(1000, 9999);
                $array['country_code'] = isset($requested_data['country_code']) ? $requested_data['country_code'] : "+91";
                $array['phone_no'] = $request['phone_no'];
                $array['name'] = $user['fname']." ".$user['lname'];
                // $result = $this->sendSms($array);
                if(isset($result) &&  $result != true)
                {
                $data = \Config::get('error.user_created');
                $data['data'] = $result;
                $data['user'] = $user;
                return Response::json($data);
                }
               
                User::where('id',$user['id'])->update($array);
                $user =User::find($request->id);
                $data['message'] = 'A verification OTP has been sent to your registered number, please verify';
                $data['status'] = 200;
                $data['user'] = $user;
                return Response::json($data);
            } else {
                $data['message'] = 'Error';
                $data['status'] = 400;
                return Response::json($data);
            }
    }
 


}
