<?php

namespace App\Jobs;

use App\Mail\AddAdminTournament;
use App\User;
use App\Models\Tournament;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Mail;

class AddCustomJob implements ShouldQueue
{

    use Dispatchable,
    InteractsWithQueue,
    Queueable,
        SerializesModels;

    public $emails,$tournament_id;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($emails,$tournament_id)
    {
        //
        $this->tournament_id = $tournament_id;
        $this->emails = $emails;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {   
       
        $tournament = Tournament::find($this->tournament_id);
        Mail::to('no-reply@abc.com')
        ->bcc($this->emails)
        ->send(new AddAdminTournament($tournament));
        
        
       
        if (count(Mail::failures()) > 0) {
            return false;
        }
        return true;
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
        Log::info($exception);
    }

}
